/**
 * 
 */
package com.pgichure.springsamples.microservices.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.pgichure.springsamples.microservices.dtos.ProductCategoryDto;
import com.pgichure.springsamples.microservices.models.ProductCategory;
import com.pgichure.springsamples.microservices.repositories.ProductCategoryRepository;

import lombok.RequiredArgsConstructor;

@Service 
@RequiredArgsConstructor
public class ProductCategoryService implements ProductCategoryServiceI{

    private final ProductCategoryRepository repository;

    @Override
    public ProductCategoryDto save(ProductCategoryDto dto){
        ProductCategory category = this.getProductCategory(dto);
        category = this.repository.save(category);

        return this.getDto(category);
    }

    @Override
    public List<ProductCategoryDto> findAll(){
        List<ProductCategory> categories = repository.findAll();

        return categories.stream()
                .map(category -> this.getDto(category))
                .collect(Collectors.toList());
    }

    @Override
    public ProductCategoryDto update(Long id, ProductCategoryDto dto) throws Exception{
        ProductCategory category = repository.findById(id).orElseThrow(() -> new Exception("Product Category not found for ID " + id));
        category = this.getProductCategory(dto);
        category = repository.save(category);
        return this.getDto(category);
    }

    @Override
    public ProductCategoryDto findById(Long id) throws Exception{
        ProductCategory category = repository.findById(id).orElseThrow(() -> new Exception("Product Category not found for ID " + id));
        return this.getDto(category);
    }

    @Override
    public ProductCategoryDto delete(Long id) throws Exception{
        ProductCategory category = repository.findById(id).orElseThrow(() -> new Exception("Product Category not found for ID " + id));
        ProductCategoryDto dto = this.getDto(category);
        repository.deleteById(id);
        return dto;
    }

    private ProductCategoryDto getDto(ProductCategory category){
        return ProductCategoryDto.builder()
                   .code(category.getCode())
                    .id(category.getId())
                    .name(category.getName())
                    .build();
    }

    private ProductCategory getProductCategory(ProductCategoryDto dto){
        return ProductCategory.builder()
                    .code(dto.getCode())
                    .id(dto.getId())
                    .name(dto.getName())
                    .build();
    }

 
}