/**
 * 
 */
package com.pgichure.springsamples.microservices.services;

import java.util.List;

import com.pgichure.springsamples.microservices.dtos.ProductDto;

public interface ProductServiceI{
 
    public ProductDto save(ProductDto product);

    public List<ProductDto> findAll();

    public ProductDto update(Long id, ProductDto product) throws Exception;

    public ProductDto findById(Long id) throws Exception;

    public ProductDto delete(Long id) throws Exception;
    
}