package com.pgichure.springsamples.microservices.dtos;

import java.math.BigDecimal;
import java.util.UUID;

import com.pgichure.springsamples.microservices.enums.UnitOfMeasure;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
 
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class ProductDto{

    private UUID id;

    private String code;
  
    private String name;

    private BigDecimal price;

    private UnitOfMeasure unitOfMeasure;

    private Long categoryId;
    
}