/**
 * 
 */
package com.pgichure.springsamples.microservices.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.pgichure.springsamples.microservices.dtos.ProductDto;
import com.pgichure.springsamples.microservices.models.Product;
import com.pgichure.springsamples.microservices.repositories.ProductCategoryRepository;
import com.pgichure.springsamples.microservices.repositories.ProductRepository;

import lombok.RequiredArgsConstructor;

@Service 
@RequiredArgsConstructor
public class ProductService implements ProductServiceI{

    private final ProductRepository repository;

    private final ProductCategoryRepository repository;

    @Override
    public ProductDto save(ProductDto dto){
        Product product = this.getProduct(dto);
        product = this.repository.save(product);

        return this.getDto(product);
    }

    @Override
    public List<ProductDto> findAll(){
        List<Product> products = repository.findAll();

        return products.stream()
                .map(product -> this.getDto(product))
                .collect(Collectors.toList());
    }

    @Override
    public ProductDto update(Long id, ProductDto dto) throws Exception{
        Product product = repository.findById(id).orElseThrow(() -> new Exception("Product not found for ID " + id));
        product = this.getOrder(dto);
        product = repository.save(product);
        return this.getDto(product);
    }

    @Override
    public ProductDto findById(Long id) throws Exception{
        Product product = repository.findById(id).orElseThrow(() -> new Exception("Product not found for ID " + id));
        return this.getDto(product);
    }

    @Override
    public ProductDto delete(Long id) throws Exception{
        Product product = repository.findById(id).orElseThrow(() -> new Exception("Product not found for ID " + id));
        ProductDto dto = this.getDto(product);
        repository.deleteById(id);
        return dto;
    }

    private ProductDto getDto(Product order){
        return ProductDto.builder()
                    .code(order.getCode())
                    .id(order.getId())
                    .name(order.getName())
                    .price(order.getPrice())
                    .unitOfMeasure(order.getUnitOfMeasure())
                    .categoryId(order.getCategoryId())
                    .build();
    }

    private Product getOrder(ProductDto dto){
        return Product.builder()
                    .code(dto.getCode())
                    .id(dto.getId())
                    .name(dto.getName())
                    .price(dto.getPrice())
                    .unitOfMeasure(dto.getUnitOfMeasure())
                    .categoryId(dto.getCategoryId())
                    .build();
    }

 
}