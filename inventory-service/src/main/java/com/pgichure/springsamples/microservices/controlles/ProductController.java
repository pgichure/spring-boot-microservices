package com.pgichure.springsamples.microservices.controllers;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pgichure.springsamples.microservices.dtos.ProductDto;
import com.pgichure.springsamples.microservices.services.ProductServiceI;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;


@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
@Api(tags = {"Products Management"}, description = "Operations on products management")
public class ProductController{

    private final ProductServiceI service;

    @PostMapping
    @ApiOperation(value = "Save a product", response = ProductDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully saved the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public ResponseEntity<ProductDto> save(@RequestBody ProductDto product){
    	product = service.save(product);
        return new ResponseEntity(product, HttpStatus.OK);
    }

    @GetMapping
    @ApiOperation(value = "Fetch products listing", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the records"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<List<ProductDto>> getAll(){
    	List<ProductDto> products = service.findAll();
        return new ResponseEntity(products, HttpStatus.OK);
    }

    @GetMapping(value ="/{id}")
    @ApiOperation(value = "Fetch product by Id", response = ProductDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<ProductDto> findById(@PathVariable Long id) throws Exception{
    	ProductDto product = service.findById(id);
        return new ResponseEntity(product, HttpStatus.OK);
    }   

    @PutMapping(value ="/{id}")
    @ApiOperation(value = "Update product by Id", response = ProductDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully updated the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<ProductDto> update(@PathVariable Long id, @RequestBody ProductDto product) throws Exception{
    	ProductDto dto = service.update(id, product);
        return new ResponseEntity(dto, HttpStatus.OK);
    }   
    

    @DeleteMapping(value ="/{id}")
    @ApiOperation(value = "Delete product by Id", response = ProductDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<ProductDto> delete(@PathVariable Long id) throws Exception{
    	ProductDto product = service.delete(id);
        return new ResponseEntity(product, HttpStatus.OK);
    }   

}