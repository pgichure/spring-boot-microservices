/**
 * 
 */
package com.pgichure.springsamples.microservices.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pgichure.springsamples.microservices.models.ProductCategory;

public interface ProductCategoryRepository extends JpaRepository<ProductCategory, UUID>{

}