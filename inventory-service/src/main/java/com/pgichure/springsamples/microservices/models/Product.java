package com.pgichure.springsamples.microservices.models;

import java.math.BigDecimal;
import java.util.UUID;

import com.pgichure.springsamples.microservices.enums.UnitOfMeasure;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
 
@Data
@Builder
@Entity
@Table(name="products") 
@AllArgsConstructor
@NoArgsConstructor
public class Product{

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name="code")
    private String code;
  
    @Column(name="name")
    private String name;

    @Column(name="price")
    private BigDecimal price;

    @Column(name="uom")
    private UnitOfMeasure unitOfMeasure;

    @ManyToOne(name="category_id")
    private ProductCategory category;
    
}