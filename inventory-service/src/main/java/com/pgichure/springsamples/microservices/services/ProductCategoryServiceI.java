/**
 * 
 */
package com.pgichure.springsamples.microservices.services;

import java.util.List;

import com.pgichure.springsamples.microservices.dtos.ProductCategoryDto;

public interface ProductCategoryServiceI{
 
    public ProductCategoryDto save(ProductCategoryDto category);

    public List<ProductCategoryDto> findAll();

    public ProductCategoryDto update(Long id, ProductCategoryDto category) throws Exception;

    public ProductCategoryDto findById(Long id) throws Exception;

    public ProductCategoryDto delete(Long id) throws Exception;
    
}