/**
 * 
 */
package com.pgichure.springsamples.microservices.controllers;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pgichure.springsamples.microservices.dtos.ProductCategoryDto;
import com.pgichure.springsamples.microservices.services.ProductCategoryServiceI;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;


@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
@Api(tags = {"Products Management"}, description = "Operations on product categories management")
public class ProductCategoryController{

    private final ProductCategoryServiceI service;

    @PostMapping
    @ApiOperation(value = "Save a product categories", response = ProductCategoryDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully saved the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public ResponseEntity<ProductCategoryDto> save(@RequestBody ProductCategoryDto category){
    	category = service.save(category);
        return new ResponseEntity(category, HttpStatus.OK);
    }

    @GetMapping
    @ApiOperation(value = "Fetch product categories listing", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the records"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<List<ProductCategoryDto>> getAll(){
    	List<ProductCategoryDto> categories = service.findAll();
        return new ResponseEntity(categories, HttpStatus.OK);
    }

    @GetMapping(value ="/{id}")
    @ApiOperation(value = "Fetch product category by Id", response = ProductCategoryDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<ProductCategoryDto> findById(@PathVariable Long id) throws Exception{
    	ProductCategoryDto category = service.findById(id);
        return new ResponseEntity(category, HttpStatus.OK);
    }   

    @PutMapping(value ="/{id}")
    @ApiOperation(value = "Update product category by Id", response = ProductCategoryDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully updated the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<ProductCategoryDto> update(@PathVariable Long id, @RequestBody ProductCategoryDto category) throws Exception{
    	ProductCategoryDto dto = service.update(id, category);
        return new ResponseEntity(dto, HttpStatus.OK);
    }   
    

    @DeleteMapping(value ="/{id}")
    @ApiOperation(value = "Delete product category by Id", response = ProductCategoryDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<ProductCategoryDto> delete(@PathVariable Long id) throws Exception{
    	ProductCategoryDto category = service.delete(id);
        return new ResponseEntity(category, HttpStatus.OK);
    }   

}