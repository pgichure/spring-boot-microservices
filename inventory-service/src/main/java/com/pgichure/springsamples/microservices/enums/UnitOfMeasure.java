package com.pgichure.springsamples.microservices.enums;

import lombok.Getter;

@Getter
public enum UnitOfMeasure{

    KILOGGRAM,LITRE, GRAM, UNIT;

}