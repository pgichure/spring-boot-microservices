package com.pgichure.springsamples.microservices.models;


import om.pgichure.springsamples.microservices.enums.OrderType;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
 
@Data
@Builder
@Entity
@Table(name="orders") 
@AllArgsConstructor
@NoArgsConstructor
public class Order{

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name="order_date")
    private LocalDate orderDate;
  
    @Column(name="customer_id", nullable = true)
    private Long customerId;

    @Column(name="supplier_id", nullable = true)
    private Long suplierId;

    @Column(name="product_id")
    private Long productId;

    @Column(name="units")
    private int units;
    
    @Column(name="remarks")
    private String remarks;

    @Column(name="unit_price")
    private BigDecimal unitPrice;
  
    @Column(name="gross_total")
    private BigDecimal grossTotal;
  
    @Column(name="tax")
    private BigDecimal tax;

    @Column(name="discount")
    private BigDecimal discount;

    @Column(name="net_total")
    private BigDecimal netTotal;  

    @Column(name="order_type")
    @Enumerated(EnumType.SSTRING)
    private OrderType orderType; 
    
}