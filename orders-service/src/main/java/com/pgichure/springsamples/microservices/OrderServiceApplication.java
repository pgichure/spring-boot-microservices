/**
 * 
 */
package com.pgichure.springsamples.microservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author pgichure
 *
 */
@SpringBootApplication
public class OrderServiceApplication {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(OrderServiceApplication.class, args);
	}
	
}