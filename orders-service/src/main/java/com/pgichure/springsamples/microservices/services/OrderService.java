/**
 * 
 */
package com.pgichure.springsamples.microservices.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.pgichure.springsamples.microservices.models.Order;
import com.pgichure.springsamples.microservices.repositories.OrderRepository;

import lombok.RequiredArgsConstructor;

@Service 
@RequiredArgsConstructor
public class OrderService implements OrderServiceI{

    private final OrderRepository repository;

    @Override
    public OrderDto save(OrderDto dto){
        Order order = this.getOrder(dto);
        order = this.repository.save(order);

        return this.getDto(order);
    }

    @Override
    public List<OrderDto> findAll(){
        List<Order> orders = repository.findAll();

        return orders.stream()
                .map(order -> this.getDto(order))
                .collect(Collectors.toList());
    }

    @Override
    public OrderDto update(Long id, OrderDto dto) throws Exception{
        Order order = repository.findById(id).orElseThrow(() -> new Exception("Order not found for ID " + id));
        order = this.getOrder(dto);
        order = repository.save(order);
        return this.getDto(order);
    }

    @Override
    public OrderDto findById(Long id) throws Exception{
        Order order = repository.findById(id).orElseThrow(() -> new Exception("Order not found for ID " + id));
        return this.getDto(order);
    }

    @Override
    public OrderDto delete(Long id) throws Exception{
        Order order = repository.findById(id).orElseThrow(() -> new Exception("Order not found for ID " + id));
        OrderDto dto = this.getDto(order);
        repository.deleteById(id);
        return dto;
    }

    private OrderDto getDto(Order order){
        return OrderDto.builder()
                    .orderDate(order.getOrderDate())
                    .id(order.getId())
                    .customerId(order.getCustomerId())
                    .productId(order.getProductId())
                    .units(order.getUnits())
                    .remarks(order.getRemarks())
                    .unitPrice(order.getUnitPrice())
                    .grossTotal(order.getGrossTotal())
                    .tax(order.getTax())
                    .discount(order.getDiscount())
                    .netTotal(order.getNetTotal())
                    .build();
    }

    private Order getOrder(OrderDto dto){
        return Order.builder()
                    .orderDate(dto.getOrderDate())
                    .id(dto.getId())
                    .customerId(dto.getCustomerId())
                    .productId(dto.getProductId())
                    .units(dto.getUnits())
                    .remarks(dto.getRemarks())
                    .unitPrice(dto.getUnitPrice())
                    .grossTotal(dto.getGrossTotal())
                    .tax(dto.getTax())
                    .discount(dto.getDiscount())
                    .netTotal(dto.getNetTotal())
                    .build();
    }

 
}