/**
 * 
 */
package com.pgichure.springsamples.microservices.services;

import java.util.List;

import com.pgichure.springsamples.microservices.dtos.OrderDto;

public interface OrderServiceI{
 
    public OrderDto save(OrderDto order);

    public List<OrderDto> findAll();

    public OrderDto update(Long id, OrderDto order) throws Exception;

    public OrderDto findById(Long id) throws Exception;

    public OrderDto delete(Long id) throws Exception;
    
}