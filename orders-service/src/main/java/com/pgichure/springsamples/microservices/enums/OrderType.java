package com.pgichure.springsamples.microservices.enums;


import lombok.Getter;

@Getter
public enum OrderType{

    IN, OUT;

}