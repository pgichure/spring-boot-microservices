package com.pgichure.springsamples.microservices.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
 
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto{

    private UUID id;

    private LocalDate orderDate;
  
    private Long customerId;

    private Long productId;

    private Integer units;
    
    private String remarks;

    private BigDecimal unitPrice;
  
    private BigDecimal grossTotal;
  
    private BigDecimal tax;

    private BigDecimal discount;

    private BigDecimal netTotal;  
  
}