/**
 * 
 */
package com.pgichure.springsamples.microservices.controllers;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pgichure.springsamples.microservices.dtos.OrderDto;
import com.pgichure.springsamples.microservices.services.OrderServiceI;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;


@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
@Api(tags = {"Orders Management"}, description = "Operations on orders management")
public class OrderController{

    private final OrderServiceI service;

    @PostMapping
    @ApiOperation(value = "Save a order", response = OrderDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully saved the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public ResponseEntity<OrderDto> save(@RequestBody OrderDto order){
    	order = service.save(order);
        return new ResponseEntity(order, HttpStatus.OK);
    }

    @GetMapping
    @ApiOperation(value = "Fetch orders listing", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the records"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<List<OrderDto>> getAll(){
    	List<OrderDto> orders = service.findAll();
        return new ResponseEntity(orders, HttpStatus.OK);
    }

    @GetMapping(value ="/{id}")
    @ApiOperation(value = "Fetch order by Id", response = OrderDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<OrderDto> findById(@PathVariable Long id) throws Exception{
    	OrderDto order = service.findById(id);
        return new ResponseEntity(order, HttpStatus.OK);
    }   

    @PutMapping(value ="/{id}")
    @ApiOperation(value = "Update order by Id", response = OrderDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully updated the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<OrderDto> update(@PathVariable Long id, @RequestBody OrderDto order) throws Exception{
    	OrderDto dto = service.update(id, order);
        return new ResponseEntity(dto, HttpStatus.OK);
    }   
    

    @DeleteMapping(value ="/{id}")
    @ApiOperation(value = "Delete order by Id", response = OrderDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<OrderDto> delete(@PathVariable Long id) throws Exception{
    	OrderDto order = service.delete(id);
        return new ResponseEntity(order, HttpStatus.OK);
    }   

}