package com.pgichure.springsamples.microservices.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pgichure.springsamples.microservices.models.Order;
 
public interface OrderRepository extends JpaRepository<Order, UUID> {
 
}