/**
 * 
 */
package com.pgichure.springsamples.microservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author pgichure
 *
 */
@SpringBootApplication
public class MicroservicesApplication {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(MicroservicesApplication.class, args);
	}
	
}