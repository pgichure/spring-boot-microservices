/**
 * 
 */
package com.pgichure.springsamples.microservices.services;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.pgichure.springsamples.microservices.dtos.ReceiptDto;
import com.pgichure.springsamples.microservices.models.Receipt;
import com.pgichure.springsamples.microservices.repositories.ReceiptRepository;

import lombok.RequiredArgsConstructor;

@Service 
@RequiredArgsConstructor
public class ReceiptService implements ReceiptServiceI{
    
    private final ReceiptRepository repository;

    @Override
    public ReceiptDto save(ReceiptDto dto){
        Receipt receipt = this.getReceipt(dto);
        receipt = this.repository.save(receipt);
        return this.getDto(receipt);
    }

    @Override
    public List<ReceiptDto> findAll(){
    	
        List<Receipt> receipts = repository.findAll();

        return receipts.stream()
                .map(receipt -> this.getDto(receipt))
                .collect(Collectors.toList());
    }

    @Override
    public ReceiptDto update(UUID id, ReceiptDto dto) throws Exception{
        Receipt receipt = repository.findById(id).orElseThrow(() -> new Exception("Receipt not found for ID " + id));
        receipt = this.getReceipt(dto);
        receipt = repository.save(receipt);
        return this.getDto(receipt);
    }

    @Override
    public ReceiptDto findById(UUID id) throws Exception{
        Receipt receipt = repository.findById(id).orElseThrow(() -> new Exception("Receipt not found for ID " + id));
        return this.getDto(receipt);
    }

    @Override
    public ReceiptDto delete(UUID id) throws Exception{
        Receipt receipt = repository.findById(id).orElseThrow(() -> new Exception("Receipt not found for ID " + id));
        ReceiptDto dto = this.getDto(receipt);
        repository.deleteById(id);
        return dto;
    }

    private ReceiptDto getDto(Receipt receipt){
        return ReceiptDto.builder()
                    .receiptDate(receipt.getReceiptDate())
                    .id(receipt.getId())
                    .orderId(receipt.getOrderId())
                    .amount(receipt.getAmount())
                    .referenceNumber(receipt.getReferenceNumber())
                    .build();
    }

    private Receipt getReceipt(ReceiptDto dto){
        return Receipt.builder()
                    .receiptDate(dto.getReceiptDate())
                    .id(dto.getId())
                    .orderId(dto.getOrderId())
                    .amount(dto.getAmount())
                    .referenceNumber(dto.getReferenceNumber())
                    .build();
    }

}