/**
 * 
 */
package com.pgichure.springsamples.microservices.controllers;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pgichure.springsamples.microservices.services.ReceiptServiceI;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;


@RestController
@RequestMapping("/receipts")
@RequiredArgsConstructor
@Api(tags = {"Receipts Management"}, description = "Operations on receipts management")
public class ReceiptController{

    private final ReceiptServiceI service;
    
}