/**
 * 
 */
package com.pgichure.springsamples.microservices.services;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.pgichure.springsamples.microservices.dtos.BillDto;
import com.pgichure.springsamples.microservices.models.Bill;
import com.pgichure.springsamples.microservices.repositories.BillRepository;

import lombok.RequiredArgsConstructor;

@Service 
@RequiredArgsConstructor
public class BillService implements BillServiceI{
    
    private final BillRepository repository;

    @Override
    public BillDto save(BillDto dto){
        Bill bill = this.getBill(dto);
        bill = this.repository.save(bill);
        return this.getDto(bill);
    }

    @Override
    public List<BillDto> findAll(){
        List<Bill> bills = repository.findAll();

        return bills.stream()
                .map(bill -> this.getDto(bill))
                .collect(Collectors.toList());
    }

    @Override
    public BillDto update(UUID id, BillDto dto) throws Exception{
        Bill bill = repository.findById(id).orElseThrow(() -> new Exception("Bill not found for ID " + id));
        bill = this.getBill(dto);
        bill = repository.save(bill);
        return this.getDto(bill);
    }

    @Override
    public BillDto findById(UUID id) throws Exception{
        Bill bill = repository.findById(id).orElseThrow(() -> new Exception("Bill not found for ID " + id));
        return this.getDto(bill);
    }

    @Override
    public BillDto delete(UUID id) throws Exception{
        Bill bill = repository.findById(id).orElseThrow(() -> new Exception("Bill not found for ID " + id));
        BillDto dto = this.getDto(bill);
        repository.deleteById(id);
        return dto;
    }

    private BillDto getDto(Bill bill){
        return BillDto.builder()
                    .billDate(bill.getBillDate())
                    .id(bill.getId())
                    .customerId(bill.getCustomerId())
                    .suplierId(bill.getSuplierId())
                    .productId(bill.getProductId())
                    .status(bill.getStatus())
                    .netTotal(bill.getNetTotal())
                    .grossTotal(bill.getGrossTotal())
                    .tax(bill.getTax())
                    .build();
    }

    private Bill getBill(BillDto dto){
        return Bill.builder()
                    .billDate(dto.getBillDate())
                    .id(dto.getId())
                    .customerId(dto.getCustomerId())
                    .suplierId(dto.getSuplierId())
                    .productId(dto.getProductId())
                    .status(dto.getStatus())
                    .netTotal(dto.getNetTotal())
                    .grossTotal(dto.getGrossTotal())
                    .tax(dto.getTax())
                    .build();
    }

}