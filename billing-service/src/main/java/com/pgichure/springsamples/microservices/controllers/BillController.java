/**
 * 
 */
package com.pgichure.springsamples.microservices.controllers;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pgichure.springsamples.microservices.services.BillServiceI;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;


@RestController
@RequestMapping("/bills")
@RequiredArgsConstructor
@Api(tags = {"Bills Management"}, description = "Operations on bills management")
public class BillController{
    
    private final BillServiceI service;
    
}