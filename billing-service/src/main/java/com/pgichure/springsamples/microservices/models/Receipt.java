/**
 * 
 */
package com.pgichure.springsamples.microservices.models;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@Table(name="receipts") 
@AllArgsConstructor
@NoArgsConstructor
public class Receipt{

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name="receipt_date")
    private LocalDate receiptDate;
  
    @Column(name="order_id")
    private Long orderId;

    @Column(name="amount")
    private BigDecimal amount;
  
    @Column(name="reference_no")
    private String referenceNumber;
  
}