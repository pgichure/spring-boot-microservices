/**
 * 
 */
package com.pgichure.springsamples.microservices.dtos;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import com.pgichure.springsamples.microservices.enums.BillStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BillDto{
    
    private UUID id;

    private LocalDate billDate;
  
    private Long customerId;

    private Long suplierId;

    private Long productId;

    private BillStatus status;

    private BigDecimal netTotal;
  
    private BigDecimal grossTotal;
  
    private BigDecimal tax;
  
}