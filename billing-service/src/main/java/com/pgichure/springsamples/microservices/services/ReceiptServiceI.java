/**
 * 
 */
package com.pgichure.springsamples.microservices.services;

import java.util.List;
import java.util.UUID;

import com.pgichure.springsamples.microservices.dtos.ReceiptDto;

public interface ReceiptServiceI{
 
    public ReceiptDto save(ReceiptDto receipt);

    public List<ReceiptDto> findAll();

    public ReceiptDto update(UUID id, ReceiptDto receipt) throws Exception;

    public ReceiptDto findById(UUID id) throws Exception;

    public ReceiptDto delete(UUID id) throws Exception;
    
}