/**
 * 
 */
package com.pgichure.springsamples.microservices.models;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import com.pgichure.springsamples.microservices.enums.BillStatus;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@Table(name="bills") 
@AllArgsConstructor
@NoArgsConstructor
public class Bill{

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name="bill_date")
    private LocalDate billDate;
  
    @Column(name="customer_id", nullable = true)
    private Long customerId;

    @Column(name="supplier_id", nullable = true)
    private Long suplierId;

    @Column(name="product_id")
    private Long productId;

    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private BillStatus status;

    @Column(name="net_total")
    private BigDecimal netTotal;
  
    @Column(name="gross_total")
    private BigDecimal grossTotal;
  
    @Column(name="tax")
    private BigDecimal tax;
    
}