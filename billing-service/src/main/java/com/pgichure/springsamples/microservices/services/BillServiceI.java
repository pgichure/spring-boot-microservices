/**
 * 
 */
package com.pgichure.springsamples.microservices.services;


import java.util.List;
import java.util.UUID;

import com.pgichure.springsamples.microservices.dtos.BillDto;

public interface BillServiceI{
    
    public BillDto save(BillDto bill);

    public List<BillDto> findAll();

    public BillDto update(UUID id, BillDto bill) throws Exception;

    public BillDto findById(UUID id) throws Exception;

    public BillDto delete(UUID id) throws Exception;

}