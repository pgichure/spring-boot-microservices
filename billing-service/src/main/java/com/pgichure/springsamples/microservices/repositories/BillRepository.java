/**
 * 
 */
package com.pgichure.springsamples.microservices.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pgichure.springsamples.microservices.models.Bill;

public interface BillRepository extends JpaRepository<Bill, UUID>{
    
}