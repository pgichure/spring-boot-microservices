/**
 * 
 */
package com.pgichure.springsamples.microservices.dtos;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReceiptDto{
    
    private UUID id;

    private LocalDate receiptDate;
  
    private Long orderId;

    private BigDecimal amount;
  
    private String referenceNumber;

}