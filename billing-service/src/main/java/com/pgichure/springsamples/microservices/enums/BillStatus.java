package com.pgichure.springsamples.microservices.enums;

public enum BillStatus{

    DRAFT, PAID, CANCELLED;
    
}