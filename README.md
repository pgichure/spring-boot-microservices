
## Microservices using Spring Boot Project
### Introduction
A MicroService is a service built around a specific business capability which can be independently deployed. It heavily employs Domain Driven Design(DDD) pattern to decompose large enterprise systems into these services. In this project, we will use SpringBoot and SpringCloud to build micro-services. We will build an Order Management System that shall use Microservices Architecture (MSA).  
We will be creating a simple order management system (OMS) to show the MSA concept and not necessarily how OMS systems work in solving real world problems.

#### Key Components of a Microservices Architecture
Key components of a microservices architecture include:

Core Services: Each service is a self-contained unit of functionality that can be developed, tested, and deployed independently of the other services.   

Service registry: A service registry is a database of all the services in the system, along with their locations and capabilities. It allows services to discover and communicate with each other. In this project, we will using Spring Discovery for service registry.  

API Gateway: An API gateway is a single entry point for all incoming requests to the microservices. It acts as a reverse proxy, routing requests to the appropriate service and handling tasks such as authentication and rate limiting. We will using Spring Cloud Gateway as the API gateway.  

Message bus: A message bus is a messaging system that allows services to communicate asynchronously with each other. This can be done through protocols like HTTP, RabbitMQ, or Kafka.  We will use RabbitMQ in this project for the Asynchrinous communication.

Monitoring and logging: Monitoring and logging are necessary to track the health of the services and troubleshoot problems.  

Service discovery and load balancing: This component is responsible for discovering service instances and directing traffic to the appropriate service instances based on load and availability.   

Continuous integration and continuous deployment (CI/CD): To make the development and deployment process of microservices as smooth as possible, it is recommended to use a tool such as Jenkins, TravisCI, or CircleCI to automate the process of building, testing, and deploying microservices.  


### Overview

![Microservices Overview](resources/microservice.png)

### Technology Stack
-  RabbitMQ
-  Spring Boot 3.1.0
-  Spring Cloud Gateway
-  MySQL
-  Docker
-  Spring Configuration
-  Spring Discovery
-  Spring Feign
-  

We will use Dtabase Per Service Pattern.  

## Running the Project
```
cd working_directory
git clone https://gitlab.com/pgichure/spring-boot-microservices.git
cd spring-boot-microservices
mvn spring-boot-run
```