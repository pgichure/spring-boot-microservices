/**
 * 
 */
package com.pgichure.springsamples.microservices.dtos;


import java.util.UUID;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerDto{

    private UUID id;

    private String firstName;
  
    private String lastName;
    
    private String postalAddress;
    
    private String postalCode;
  
    private String town;

    private String country;
  
}