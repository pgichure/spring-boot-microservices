/**
 * 
 */
package com.pgichure.springsamples.microservices.dtos;


import java.util.UUID;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SupplierDto{

    private UUID id;

    private String name;
    
    private String postalAddress;
    
    private String postalCode;
  
    private String town;

    private String country;
  
}