/**
 * 
 */
package com.pgichure.springsamples.microservices.controllers;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pgichure.springsamples.microservices.dtos.SupplierDto;
import com.pgichure.springsamples.microservices.services.SupplierServiceI;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;


@RestController
@RequestMapping("/suppliers")
@RequiredArgsConstructor
@Api(tags = {"Suppliers Management"}, description = "Operations on suppliers management")
public class SupplierController{

    private final SupplierServiceI service;

    @PostMapping
    @ApiOperation(value = "Save a supplier", response = SupplierDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully saved the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public ResponseEntity<SupplierDto> save(@RequestBody SupplierDto supplier){
    	supplier = service.save(supplier);
        return new ResponseEntity(supplier, HttpStatus.OK);
    }

    @GetMapping
    @ApiOperation(value = "Fetch suppliers listing", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the records"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<List<SupplierDto>> getAll(){
    	List<SupplierDto> suppliers = service.findAll();
        return new ResponseEntity(suppliers, HttpStatus.OK);
    }

    @GetMapping(value ="/{id}")
    @ApiOperation(value = "Fetch supplier by Id", response = SupplierDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<SupplierDto> findById(@PathVariable Long id) throws Exception{
    	SupplierDto supplier = service.findById(id);
        return new ResponseEntity(supplier, HttpStatus.OK);
    }   

    @PutMapping(value ="/{id}")
    @ApiOperation(value = "Update supplier by Id", response = SupplierDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully updated the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<SupplierDto> update(@PathVariable Long id, @RequestBody SupplierDto supplier) throws Exception{
    	SupplierDto dto = service.update(id, supplier);
        return new ResponseEntity(dto, HttpStatus.OK);
    }   
    

    @DeleteMapping(value ="/{id}")
    @ApiOperation(value = "Delete supplier by Id", response = SupplierDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<SupplierDto> delete(@PathVariable Long id) throws Exception{
    	SupplierDto supplier = service.delete(id);
        return new ResponseEntity(supplier, HttpStatus.OK);
    }   

}