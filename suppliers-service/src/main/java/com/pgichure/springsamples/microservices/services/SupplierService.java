/**
 * 
 */
package com.pgichure.springsamples.microservices.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.pgichure.springsamples.microservices.dtos.SupplierDto;
import com.pgichure.springsamples.microservices.models.Supplier;
import com.pgichure.springsamples.microservices.repositories.SupplierRepository;

import lombok.RequiredArgsConstructor;

@Service 
@RequiredArgsConstructor
public class SupplierService implements SupplierServiceI{

    private final SupplierRepository repository;

    @Override
    public SupplierDto save(SupplierDto dto){
        Supplier supplier = this.getCustomer(dto);
        supplier = this.repository.save(supplier);
        return this.getDto(supplier);
    }

    @Override
    public List<SupplierDto> findAll(){
        List<Supplier> suppliers = repository.findAll();

        return suppliers.stream()
                .map(supplier -> this.getDto(supplier))
                .collect(Collectors.toList());
    }

    @Override
    public SupplierDto update(Long id, SupplierDto dto) throws Exception{
        Supplier supplier = repository.findById(id).orElseThrow(() -> new Exception("Supplier not found for ID " + id));
        supplier = this.getSupplier(dto);
        supplier = repository.save(supplier);
        return this.getDto(supplier);
    }

    @Override
    public SupplierDto findById(Long id) throws Exception{
        Supplier supplier = repository.findById(id).orElseThrow(() -> new Exception("Supplier not found for ID " + id));
        return this.getDto(supplier);
    }

    @Override
    public SupplierDto delete(Long id) throws Exception{
        Supplier supplier = repository.findById(id).orElseThrow(() -> new Exception("Supplier not found for ID " + id));
        SupplierDto dto = this.getDto(supplier);
        repository.deleteById(id);
        return dto;
    }

    private SupplierDto getDto(Supplier supplier){

        return SupplierDto.builder()
                          .firstName(supplier.getFirstName())
                          .id(supplier.getId())
                          .lastName(supplier.getLastName())
                          .postalAddress(supplier.getPostalAddress())
                          .postalCode(supplier.getPostalCode())
                          .town(supplier.getTown())
                          .country(supplier.getCountry())
                          .build();
    }

    private Supplier getCustomer(SupplierDto dto){
        return Supplier.builder()
                          .firstName(dto.getFirstName())
                          .id(dto.getId())
                          .lastName(dto.getLastName())
                          .postalAddress(dto.getPostalAddress())
                          .postalCode(dto.getPostalCode())
                          .town(dto.getTown())
                          .country(dto.getCountry())
                          .build();
    }

 
}