/**
 * 
 */
package com.pgichure.springsamples.microservices.models;


import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
 
@Data
@Builder
@Entity
@Table(name="suppliers") 
@AllArgsConstructor
@NoArgsConstructor
public class Supplier{

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(name="first_name")
    private String name;
  
       @Column(name="postal_address")
    private String postalAddress;
  
    @Column(name="postal_code")
    private String postalCode;
  
    @Column(name="town")
    private String town;

    @Column(name="country")
    private String country;   
  
}