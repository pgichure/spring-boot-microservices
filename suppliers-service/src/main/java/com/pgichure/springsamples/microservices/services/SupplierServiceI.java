/**
 * 
 */
package com.pgichure.springsamples.microservices.services;

import java.util.List;

import com.pgichure.springsamples.microservices.dtos.SupplierDto;

public interface SupplierServiceI{
 
    public SupplierDto save(SupplierDto supplier);

    public List<SupplierDto> findAll();

    public SupplierDto update(Long id, SupplierDto supplier) throws Exception;

    public SupplierDto findById(Long id) throws Exception;

    public SupplierDto delete(Long id) throws Exception;
    
}